package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.proto.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Matěj Mihal
 */
public class ProtoDataHandler implements DataHandler {

    private Map<Integer, PDataset.Builder> datasets;

    private final OutputStream os;
    private final InputStream is;

    public ProtoDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void start() {
        datasets = new HashMap<>();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        PMeasurementInfo info = PMeasurementInfo.newBuilder().setId(datasetId)
                .setTimestamp(timestamp).setMeasurerName(measurerName).build();
        PDataset.Builder dataset = PDataset.newBuilder().setInfo(info);
        datasets.put(datasetId, dataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        PDataset.Builder dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        if (!dataset.getRecordsMap().containsKey(type.name())) {
            dataset.putRecords(type.name(), PRecords.newBuilder().build());
        }
        dataset.putRecords(type.name(), dataset.getRecordsMap().get(type.name()).toBuilder().addValues(value).build());
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        PDatasetArray toSend = PDatasetArray.newBuilder()
                .addAllDatasets(datasets.values().stream().map(t -> t.build()).collect(Collectors.toList())).build();
        int messageSize = toSend.getSerializedSize();
        os.write(ByteBuffer.allocate(4).putInt(messageSize).array());
        os.flush();
        toSend.writeTo(os);
        os.flush();
        PMeasurementResultsArray results = PMeasurementResultsArray.parseFrom(is);
        for (PMeasurementResult result : results.getResultsList()) {
            PMeasurementInfo info = result.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
            result.getAveragesMap().forEach((k, v) -> consumer.acceptResult(DataType.valueOf(k), v));
        }

    }
}
