package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.*;
import cz.esw.serialization.json.DataType;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Matěj Mihal
 */
public class AvroDataHandler implements DataHandler {

    private Map<Integer, ADataset> datasets;

    private final OutputStream os;
    private final InputStream is;

    private final DatumWriter<ADatasetArray> datasetsDatumWriter;

    private final DatumReader<AMResultsArray> resultsDatumReader;

    public AvroDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
        datasetsDatumWriter = new SpecificDatumWriter<>(ADatasetArray.class);
        resultsDatumReader = new SpecificDatumReader<>(AMResultsArray.class);
    }

    @Override
    public void start() {
        datasets = new HashMap<>();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        AMInfo info = AMInfo.newBuilder().setId(datasetId).setTimestamp(timestamp).setMeasurerName(measurerName).build();
        ADataset dataset = ADataset.newBuilder().setInfo(info).setRecords(new HashMap<>()).build();
        datasets.put(datasetId, dataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        ADataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        dataset.getRecords().computeIfAbsent(type.name(), t -> new ArrayList<>()).add(value);
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(byteArrayOutputStream, null);

        ADatasetArray objectToBeSerialized = ADatasetArray.newBuilder().setDatasets(new ArrayList<>(datasets.values())).build();

        datasetsDatumWriter.write(objectToBeSerialized, encoder);
        System.out.println("Data serialized");
        encoder.flush();

        int messageSize = byteArrayOutputStream.size();

        os.write(ByteBuffer.allocate(4).putInt(messageSize).array());
        os.write(byteArrayOutputStream.toByteArray());
        os.flush();
        System.out.println("Data send");


        Decoder decoder = DecoderFactory.get().binaryDecoder(is, null);
        AMResultsArray results = null;

        System.out.println("Reading results");
        results = resultsDatumReader.read(null, decoder);

        for (AMResults result : results.getResults()) {
            AMInfo info = result.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName().toString());
            result.getAverages().forEach((k,v) -> consumer.acceptResult(DataType.valueOf(k.toString()),v));
        }


    }
}
