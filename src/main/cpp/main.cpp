#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <vector>
#include <cstring>
#include <unistd.h>
#include "avromeasurements.hh"
#include "measurements.pb.h"


#include <jsoncpp/json/json.h>


#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"

//#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <bitset>

using namespace std;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

int chartoint(char* buf)
{
    bitset<32> lb;
    int value=0;
    for(int i=3;i>=0;i--)
    {
        bitset<8> t (buf[i]);
        for(int j=0;j<8;j++)
            lb[value++]=t[j];
    }
    return (int)lb.to_ulong();
}
void processAvro(tcp::iostream& stream){
    AMResultsArray response;
    ADatasetArray data;
    char * buffer = new char[4];
    stream.read(buffer,4);
    int length=chartoint(buffer);
    buffer=new char[length];
    stream.read(buffer,length);
    auto ptr=avro::binaryDecoder();
    std::unique_ptr<avro::InputStream> a=avro::memoryInputStream((uint8_t *)buffer,length);
    ptr->init(*a);
    avro::decode(*ptr,data);
    for(ADataset dataset : data.datasets)
    {
        AMResults result;
        result.info=dataset.info;
        double c=0,val=0;
        for(double v: dataset.records["DOWNLOAD"])
        {
            val+=v;
            c++;
        }
        result.averages["DOWNLOAD"]=val/c;
        val=0;c=0;
        for(double v: dataset.records["UPLOAD"])
        {
            val+=v;
            c++;
        }
        result.averages["UPLOAD"]=val/c;
        val=0;c=0;
        for(double v: dataset.records["PING"])
        {
            val+=v;
            c++;
        }
        result.averages["PING"]=val/c;
        response.results.push_back(result);
    }
    std::unique_ptr<avro::OutputStream> out=avro::ostreamOutputStream(stream);
    avro::EncoderPtr ptr2 = avro::binaryEncoder();
    ptr2->init(*out);
    avro::encode(*ptr2,response);
    ptr2->flush();

}
void processProtobuf(tcp::iostream& stream){
    esw::PMeasurementResultsArray response;
    esw::PDatasetArray data;
    char * buffer = new char[4];
    stream.read(buffer,4);
    int length=chartoint(buffer);
    buffer= new char[length];
    stream.read(buffer,length);
    data.ParseFromArray(buffer,length);
    delete(buffer);

    for(int i=0;i<data.datasets_size();i++)
    {
        esw::PDataset set=data.datasets(i);
        esw::PMeasurementResult * res=response.add_results();
        esw::PMeasurementInfo * info=new esw::PMeasurementInfo;
        info->set_timestamp(set.info().timestamp());
        info->set_measurername(set.info().measurername());
        info->set_id(set.info().id());
        res->set_allocated_info(info);
        cout << "Dataset " << res->info().id() << endl;
        double d=0,u=0,p=0;
        esw::PRecords rec=set.records().at("DOWNLOAD");
        for(double val : *rec.mutable_values()) {
            d += val;
        }
        google::protobuf::MapPair<string,double> v("DOWNLOAD",d/rec.values_size());
        res->mutable_averages()->insert(v);
        rec=set.records().at("UPLOAD");
        for(double val : *rec.mutable_values()) {
            u+=val;
        }
        google::protobuf::MapPair<string,double> v2("UPLOAD",u/rec.values_size());
        res->mutable_averages()->insert(v2);
        rec=set.records().at("PING");
        for(double val : *rec.mutable_values()) {
            p+=val;
        }
        google::protobuf::MapPair<string,double> v3("PING",p/rec.values_size());
        res->mutable_averages()->insert(v3);

    }
    response.SerializeToOstream(&stream);
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }



    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "json"){
                processJSON(stream);
            }else if(protocol == "avro"){
                processAvro(stream);
            }else if(protocol == "proto"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
