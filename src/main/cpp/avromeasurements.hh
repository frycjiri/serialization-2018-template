/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CPX_HH_3643932286__H_
#define CPX_HH_3643932286__H_


#include <sstream>
#include <vector>
#include <map>
#include "boost/any.hpp"
#include "avro/Specific.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"

struct AMInfo {
    int32_t id;
    int64_t timestamp;
    std::string measurerName;
    AMInfo() :
        id(int32_t()),
        timestamp(int64_t()),
        measurerName(std::string())
        { }
};

struct ADataset {
    AMInfo info;
    std::map<std::string, std::vector<double > > records;
    ADataset() :
        info(AMInfo()),
        records(std::map<std::string, std::vector<double > >())
        { }
};

struct ADatasetArray {
    std::vector<ADataset > datasets;
    ADatasetArray() :
        datasets(std::vector<ADataset >())
        { }
};

struct AMResults {
    AMInfo info;
    std::map<std::string, double > averages;
    AMResults() :
        info(AMInfo()),
        averages(std::map<std::string, double >())
        { }
};

struct AMResultsArray {
    std::vector<AMResults > results;
    AMResultsArray() :
        results(std::vector<AMResults >())
        { }
};

struct measurements_avsc_Union__0__ {
private:
    size_t idx_;
    boost::any value_;
public:
    size_t idx() const { return idx_; }
    AMInfo get_AMInfo() const;
    void set_AMInfo(const AMInfo& v);
    ADataset get_ADataset() const;
    void set_ADataset(const ADataset& v);
    ADatasetArray get_ADatasetArray() const;
    void set_ADatasetArray(const ADatasetArray& v);
    AMResults get_AMResults() const;
    void set_AMResults(const AMResults& v);
    AMResultsArray get_AMResultsArray() const;
    void set_AMResultsArray(const AMResultsArray& v);
    measurements_avsc_Union__0__();
};

inline
AMInfo measurements_avsc_Union__0__::get_AMInfo() const {
    if (idx_ != 0) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AMInfo >(value_);
}

inline
void measurements_avsc_Union__0__::set_AMInfo(const AMInfo& v) {
    idx_ = 0;
    value_ = v;
}

inline
ADataset measurements_avsc_Union__0__::get_ADataset() const {
    if (idx_ != 1) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADataset >(value_);
}

inline
void measurements_avsc_Union__0__::set_ADataset(const ADataset& v) {
    idx_ = 1;
    value_ = v;
}

inline
ADatasetArray measurements_avsc_Union__0__::get_ADatasetArray() const {
    if (idx_ != 2) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADatasetArray >(value_);
}

inline
void measurements_avsc_Union__0__::set_ADatasetArray(const ADatasetArray& v) {
    idx_ = 2;
    value_ = v;
}

inline
AMResults measurements_avsc_Union__0__::get_AMResults() const {
    if (idx_ != 3) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AMResults >(value_);
}

inline
void measurements_avsc_Union__0__::set_AMResults(const AMResults& v) {
    idx_ = 3;
    value_ = v;
}

inline
AMResultsArray measurements_avsc_Union__0__::get_AMResultsArray() const {
    if (idx_ != 4) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AMResultsArray >(value_);
}

inline
void measurements_avsc_Union__0__::set_AMResultsArray(const AMResultsArray& v) {
    idx_ = 4;
    value_ = v;
}

inline measurements_avsc_Union__0__::measurements_avsc_Union__0__() : idx_(0), value_(AMInfo()) { }
namespace avro {
template<> struct codec_traits<AMInfo> {
    static void encode(Encoder& e, const AMInfo& v) {
        avro::encode(e, v.id);
        avro::encode(e, v.timestamp);
        avro::encode(e, v.measurerName);
    }
    static void decode(Decoder& d, AMInfo& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.id);
                    break;
                case 1:
                    avro::decode(d, v.timestamp);
                    break;
                case 2:
                    avro::decode(d, v.measurerName);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.id);
            avro::decode(d, v.timestamp);
            avro::decode(d, v.measurerName);
        }
    }
};

template<> struct codec_traits<ADataset> {
    static void encode(Encoder& e, const ADataset& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.records);
    }
    static void decode(Decoder& d, ADataset& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.records);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.records);
        }
    }
};

template<> struct codec_traits<ADatasetArray> {
    static void encode(Encoder& e, const ADatasetArray& v) {
        avro::encode(e, v.datasets);
    }
    static void decode(Decoder& d, ADatasetArray& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.datasets);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.datasets);
        }
    }
};

template<> struct codec_traits<AMResults> {
    static void encode(Encoder& e, const AMResults& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.averages);
    }
    static void decode(Decoder& d, AMResults& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.averages);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.averages);
        }
    }
};

template<> struct codec_traits<AMResultsArray> {
    static void encode(Encoder& e, const AMResultsArray& v) {
        avro::encode(e, v.results);
    }
    static void decode(Decoder& d, AMResultsArray& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.results);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.results);
        }
    }
};

template<> struct codec_traits<measurements_avsc_Union__0__> {
    static void encode(Encoder& e, measurements_avsc_Union__0__ v) {
        e.encodeUnionIndex(v.idx());
        switch (v.idx()) {
        case 0:
            avro::encode(e, v.get_AMInfo());
            break;
        case 1:
            avro::encode(e, v.get_ADataset());
            break;
        case 2:
            avro::encode(e, v.get_ADatasetArray());
            break;
        case 3:
            avro::encode(e, v.get_AMResults());
            break;
        case 4:
            avro::encode(e, v.get_AMResultsArray());
            break;
        }
    }
    static void decode(Decoder& d, measurements_avsc_Union__0__& v) {
        size_t n = d.decodeUnionIndex();
        if (n >= 5) { throw avro::Exception("Union index too big"); }
        switch (n) {
        case 0:
            {
                AMInfo vv;
                avro::decode(d, vv);
                v.set_AMInfo(vv);
            }
            break;
        case 1:
            {
                ADataset vv;
                avro::decode(d, vv);
                v.set_ADataset(vv);
            }
            break;
        case 2:
            {
                ADatasetArray vv;
                avro::decode(d, vv);
                v.set_ADatasetArray(vv);
            }
            break;
        case 3:
            {
                AMResults vv;
                avro::decode(d, vv);
                v.set_AMResults(vv);
            }
            break;
        case 4:
            {
                AMResultsArray vv;
                avro::decode(d, vv);
                v.set_AMResultsArray(vv);
            }
            break;
        }
    }
};

}
#endif
